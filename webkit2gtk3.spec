# Filter out provides for private libraries
%global __provides_exclude_from ^(%{_libdir}/webkit2gtk-4\\.0/.*\\.so|%{_libdir}/webkit2gtk-4\\.1/.*\\.so|%{_libdir}/webkit2gtk-5\\.0/.*\\.so)$

%global _dwz_max_die_limit 250000000
%global _dwz_max_die_limit_x86_64 250000000

%global add_to_license_files() \
        mkdir -p _license_files ; \
        cp -p %1 _license_files/$(echo '%1' | sed -e 's!/!.!g')

%ifarch aarch64
%bcond_without docs
%endif

Name:           webkit2gtk3
Version:        2.38.2
Release:        8
Summary:        GTK web content engine library
License:        LGPLv2
URL:            https://www.webkitgtk.org/
Source0:        https://webkitgtk.org/releases/webkitgtk-%{version}.tar.xz
Source1:        https://webkitgtk.org/releases/webkitgtk-%{version}.tar.xz.asc

%ifarch loongarch64
Patch0001:      0001-webkitgtk-add-loongarch.patch
%endif
%ifarch sw_64
Patch0002:      webkitgtk-2.32.1-sw.patch
%endif

Patch6000:      backport-CVE-2023-28204.patch
Patch6001:      backport-CVE-2023-32373.patch
Patch6002:      backport-CVE-2023-32409.patch
Patch6003:      backport-Fix-build-with-Ruby-3.2.patch
Patch6004:      backport-CVE-2023-39928.patch

#Dependency
BuildRequires:  bison
BuildRequires:  bubblewrap
BuildRequires:  cmake
BuildRequires:  flex
BuildRequires:  gcc-c++
BuildRequires:  gettext
BuildRequires:  gi-docgen
BuildRequires:  git
BuildRequires:  gnupg2
BuildRequires:  gperf
BuildRequires:  hyphen-devel
BuildRequires:  libatomic
BuildRequires:  ninja-build
BuildRequires:  perl(English)
BuildRequires:  perl(FindBin)
BuildRequires:  perl(JSON::PP)
BuildRequires:  python3
BuildRequires:  ruby
BuildRequires:  rubygems
BuildRequires:  rubygem-json
BuildRequires:  xdg-dbus-proxy

BuildRequires:  pkgconfig(atspi-2)
BuildRequires:  pkgconfig(cairo)
BuildRequires:  pkgconfig(egl)
BuildRequires:  pkgconfig(enchant-2)
BuildRequires:  pkgconfig(fontconfig)
BuildRequires:  pkgconfig(freetype2)
BuildRequires:  pkgconfig(gl)
BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(glesv2)
BuildRequires:  pkgconfig(gobject-introspection-1.0)
BuildRequires:  pkgconfig(gstreamer-1.0)
BuildRequires:  pkgconfig(gstreamer-plugins-base-1.0)
BuildRequires:  pkgconfig(gtk+-3.0)
BuildRequires:  pkgconfig(gtk4)
BuildRequires:  pkgconfig(harfbuzz)
BuildRequires:  pkgconfig(icu-uc)
BuildRequires:  pkgconfig(lcms2)
BuildRequires:  pkgconfig(libgcrypt)
BuildRequires:  pkgconfig(libjpeg)
BuildRequires:  pkgconfig(libnotify)
BuildRequires:  pkgconfig(libopenjp2)
BuildRequires:  pkgconfig(libpcre)
BuildRequires:  pkgconfig(libpng)
BuildRequires:  pkgconfig(libseccomp)
BuildRequires:  pkgconfig(libsecret-1)
BuildRequires:  pkgconfig(libsoup-2.4)
BuildRequires:  pkgconfig(libsoup-3.0)
BuildRequires:  pkgconfig(libsystemd)
BuildRequires:  pkgconfig(libtasn1)
BuildRequires:  pkgconfig(libwebp)
BuildRequires:  pkgconfig(libwoff2dec)
BuildRequires:  pkgconfig(libxslt)
#BuildRequires:  pkgconfig(manette-0.2)
BuildRequires:  pkgconfig(sqlite3)
BuildRequires:  pkgconfig(upower-glib)
BuildRequires:  pkgconfig(wayland-client)
BuildRequires:  pkgconfig(wayland-egl)
BuildRequires:  pkgconfig(wayland-protocols)
BuildRequires:  pkgconfig(wayland-server)
BuildRequires:  pkgconfig(wpe-1.0)
BuildRequires:  pkgconfig(wpebackend-fdo-1.0)
BuildRequires:  pkgconfig(xt)

Requires:       javascriptcoregtk4.0%{?_isa} = %{version}-%{release}
Requires:       bubblewrap
Requires:       xdg-dbus-proxy
Recommends:     geoclue2
Recommends:     gstreamer1-plugins-bad-free
Recommends:     gstreamer1-plugins-good
Recommends:     xdg-desktop-portal-gtk
Provides:       bundled(angle)
Provides:       bundled(pdfjs)
Provides:       bundled(xdgmime)
Obsoletes:      webkitgtk4 < %{version}-%{release}
Provides:       webkitgtk4 = %{version}-%{release}
Obsoletes:      webkit2gtk4.0 < %{version}-%{release}
Provides:       webkit2gtk4.0 = %{version}-%{release}
Provides:       webkit2gtk4.0%{?_isa} = %{version}-%{release}
Provides:       webkit2gtk3 = %{version}-%{release}
Provides:       webkit2gtk3%{?_isa} = %{version}-%{release}

%description
WebKitGTK is the port of the WebKit web rendering engine to the
GTK platform. This package contains WebKitGTK for GTK 3 and libsoup 2.

%package -n     webkit2gtk3-devel
Summary:        Development files for webkit2gtk4.0
Requires:       webkit2gtk4.0%{?_isa} = %{version}-%{release}
Requires:       javascriptcoregtk4.0%{?_isa} = %{version}-%{release}
Requires:       javascriptcoregtk4.0-devel%{?_isa} = %{version}-%{release}
Obsoletes:      webkitgtk4-devel < %{version}-%{release}
Provides:       webkitgtk4-devel = %{version}-%{release}
Obsoletes:      webkit2gtk3-devel < %{version}-%{release}
Provides:       webkit2gtk3-devel = %{version}-%{release}

%description -n webkit2gtk3-devel
The webkit2gtk4.0-devel package contains libraries, build data, and header
files for developing applications that use webkit2gtk4.0.

%if %{with docs}
%package -n     webkit2gtk3-help
Summary:        Documentation files for webkit2gtk4.0
BuildArch:      noarch
Requires:       webkit2gtk4.0 = %{version}-%{release}
Obsoletes:      webkitgtk4-doc < %{version}-%{release}
Provides:       webkitgtk4-doc = %{version}-%{release}
Obsoletes:      webkitgtk4-help < %{version}-%{release}
Provides:       webkitgtk4-help = %{version}-%{release}
Obsoletes:      webkit2gtk3-doc < %{version}-%{release}
Provides:       webkit2gtk3-doc = %{version}-%{release}
Obsoletes:      webkit2gtk3-help < %{version}-%{release}
Provides:       webkit2gtk3-help = %{version}-%{release}

%description -n webkit2gtk3-help
This package contains developer documentation for webkit2gtk4.0.
%endif

%package -n     webkit2gtk3-jsc
Summary:        JavaScript engine from webkit2gtk3
Provides:       javascriptcoregtk4.0%{?_isa} = %{version}-%{release}
Obsoletes:      webkitgtk4-jsc < %{version}-%{release}
Provides:       webkitgtk4-jsc = %{version}-%{release}
Obsoletes:      webkit2gtk3-jsc < %{version}-%{release}
Provides:       webkit2gtk3-jsc = %{version}-%{release}
Obsoletes:      jsc4.0 < %{version}-%{release}
Provides:       jsc4.0 = %{version}-%{release}
Provides:       jsc4.0%{?_isa} = %{version}-%{release}

%description -n webkit2gtk3-jsc
This package contains JavaScript engine from webkit2gtk4.0.

%package -n     webkit2gtk3-jsc-devel
Summary:        Development files for JavaScript engine from webkit2gtk4.0
Provides:       javascriptcoregtk4.0-devel%{?_isa} = %{version}-%{release}
Requires:       javascriptcoregtk4.0%{?_isa} = %{version}-%{release}
Obsoletes:      webkitgtk4-jsc-devel < %{version}-%{release}
Provides:       webkitgtk4-jsc-devel = %{version}-%{release}
Obsoletes:      webkit2gtk3-jsc-devel < %{version}-%{release}
Provides:       webkit2gtk3-jsc-devel = %{version}-%{release}
Obsoletes:      jsc4.0-devel < %{version}-%{release}
Provides:       jsc4.0-devel = %{version}-%{release}
Provides:       jsc4.0-devel%{?_isa} = %{version}-%{release}

%description -n webkit2gtk3-jsc-devel
The javascriptcoregtk4.0-devel package contains libraries, build data, and header
files for developing applications that use JavaScript engine from webkit2gtk-4.0.

%prep
%autosetup -p1 -n webkitgtk-%{version}

%build
#%%define _vpath_builddir %{_vendor}-%{_target_os}-build/webkit2gtk-4.0
mkdir -p build-4.0
pushd build-4.0
%cmake \
  -GNinja \
  -DPORT=GTK \
  -DCMAKE_BUILD_TYPE=Release \
  -DUSE_SOUP2=ON \
  -DENABLE_WEBDRIVER=OFF \
%if %{without docs}
  -DENABLE_DOCUMENTATION=OFF \
%endif
  -DENABLE_GAMEPAD=OFF \
%ifarch aarch64 loongarch64
  -DENABLE_JIT=OFF \
  -DUSE_SYSTEM_MALLOC=ON \
%endif
%if 0%{?openEuler}
%ifarch aarch64
  -DUSE_64KB_PAGE_BLOCK=ON \
%endif
%endif
  ..
  %{nil}
export NINJA_STATUS="[3/3][%f/%t %es] "
%ninja_build -j16
popd

%install
#%%define _vpath_builddir %{_vendor}-%{_target_os}-build/webkit2gtk-4.0
pushd build-4.0
%ninja_install
popd

%find_lang WebKit2GTK-4.0

# Finally, copy over and rename various files for %%license inclusion
%add_to_license_files Source/JavaScriptCore/COPYING.LIB
%add_to_license_files Source/ThirdParty/ANGLE/LICENSE
%add_to_license_files Source/ThirdParty/ANGLE/src/common/third_party/smhasher/LICENSE
%add_to_license_files Source/ThirdParty/ANGLE/src/third_party/libXNVCtrl/LICENSE
%add_to_license_files Source/WebCore/LICENSE-APPLE
%add_to_license_files Source/WebCore/LICENSE-LGPL-2
%add_to_license_files Source/WebCore/LICENSE-LGPL-2.1
%add_to_license_files Source/WebInspectorUI/UserInterface/External/CodeMirror/LICENSE
%add_to_license_files Source/WebInspectorUI/UserInterface/External/Esprima/LICENSE
%add_to_license_files Source/WebInspectorUI/UserInterface/External/three.js/LICENSE
%add_to_license_files Source/WTF/icu/LICENSE
%add_to_license_files Source/WTF/wtf/dtoa/COPYING
%add_to_license_files Source/WTF/wtf/dtoa/LICENSE

%files -n webkit2gtk3 -f WebKit2GTK-4.0.lang
%license _license_files/*ThirdParty*
%license _license_files/*WebCore*
%license _license_files/*WebInspectorUI*
%license _license_files/*WTF*
%{_libdir}/libwebkit2gtk-4.0.so.37*
%dir %{_libdir}/girepository-1.0
%{_libdir}/girepository-1.0/WebKit2-4.0.typelib
%{_libdir}/girepository-1.0/WebKit2WebExtension-4.0.typelib
%{_libdir}/webkit2gtk-4.0/
%{_libexecdir}/webkit2gtk-4.0/
%exclude %{_libexecdir}/webkit2gtk-4.0/MiniBrowser
%exclude %{_libexecdir}/webkit2gtk-4.0/jsc

%files -n webkit2gtk3-devel
%{_libexecdir}/webkit2gtk-4.0/MiniBrowser
%{_includedir}/webkitgtk-4.0/
%exclude %{_includedir}/webkitgtk-4.0/JavaScriptCore
%exclude %{_includedir}/webkitgtk-4.0/jsc
%{_libdir}/libwebkit2gtk-4.0.so
%{_libdir}/pkgconfig/webkit2gtk-4.0.pc
%{_libdir}/pkgconfig/webkit2gtk-web-extension-4.0.pc
%dir %{_datadir}/gir-1.0
%{_datadir}/gir-1.0/WebKit2-4.0.gir
%{_datadir}/gir-1.0/WebKit2WebExtension-4.0.gir

%files -n webkit2gtk3-jsc
%license _license_files/*JavaScriptCore*
%{_libdir}/libjavascriptcoregtk-4.0.so.18*
%dir %{_libdir}/girepository-1.0
%{_libdir}/girepository-1.0/JavaScriptCore-4.0.typelib

%files -n webkit2gtk3-jsc-devel
%{_libexecdir}/webkit2gtk-4.0/jsc
%dir %{_includedir}/webkitgtk-4.0
%{_includedir}/webkitgtk-4.0/JavaScriptCore/
%{_includedir}/webkitgtk-4.0/jsc/
%{_libdir}/libjavascriptcoregtk-4.0.so
%{_libdir}/pkgconfig/javascriptcoregtk-4.0.pc
%dir %{_datadir}/gir-1.0
%{_datadir}/gir-1.0/JavaScriptCore-4.0.gir

%if %{with docs}
%files -n webkit2gtk3-help
%dir %{_datadir}/gtk-doc
%dir %{_datadir}/gtk-doc/html
%{_datadir}/gtk-doc/html/javascriptcoregtk-4.0/
%{_datadir}/gtk-doc/html/webkit2gtk-4.0/
%{_datadir}/gtk-doc/html/webkit2gtk-web-extension-4.0/
%endif

%changelog
* Wed Oct 11 2023 zhouwenpei <zhouwenpei1@h-partners.com> - 2.38.2-8
- fix check_install error

* Sun Oct 08 2023 zhouwenpei <zhouwenpei1@h-partners.com> - 2.38.2-7
- fix CVE-2023-39928

* Thu Aug 10 2023 xiasenlin <xiasenlin1@huawei.com> - 2.38.2-6
- split webkit2gtk3 from webkit2gtk3

* Mon May 29 2023 zhangpan <zhangpan103@h-partners.com> - 2.38.2-4
- fix CVE-2023-28204 CVE-2023-32373 CVE-2023-32409

* Fri Mar 17 2023 zhouwenpei <zhouwenpei1@h-partners.com> - 2.38.2-3
- strip binary files

* Thu Feb 16 2023 wenlong ding <wenlong.ding@turbolinux.com.cn> - 2.38.2-2
- Rename package to adaptor old Version

* Mon Dec 05 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 2.38.2-1
- Update to 2.38.2 for gnome 43

* Tue Nov 29 2022 wuzx<wuzx1226@qq.com> - 2.36.3-3
- Add sw64 architecture

* Mon Nov 14 2022 huajingyun <huajingyun@loongson.cn> 2.36.3-2
- Add support loongarch

* Mon Jun 13 2022 lin zhang <lin.zhang@turbolinux.com.cn> 2.36.3-1
- Update to 2.36.3

* Fri Jun 10 2022 zhujunhao<zhujunhao11@huawei.com> - 2.32.4-4
- add wayland-porotocols-devel buildrequires

* Tue Jun 07 2022 houjinchang<houjinchang@huawei.com> - 2.32.4-3
- fix CVE-2022-30293 and CVE-2022-30294

* Thu Nov 04 2021 liuyumeng<liuyumeng5@huawei.com> - 2.32.4-2
- fix CVE-2021-42762

* Fri Oct 22 2021 zhanzhimin<zhanzhimin@huawei.com> - 2.32.4-1
- upgrade to 2.32.4

* Thu Jul 29 2021 wangkerong<wangkerong@huawei.com> - 2.32.1-2
- change xdg-desktop-protal-gts dependences

* Mon Jun 21 2021 wangkerong<wangkerong@huawei.com> - 2.32.1-1
- upgrade to 2.32.1

* Tue Dec 15 2020 hanhui<hanhui15@huawei.com> - 2.28.3-3
- modify license

* Wed Aug 05 2020 songnannan <songnannan2@huawei.com> - 2.28.3-2
- change the mesa-libELGS-devel to libglvnd-devel

* Thu Jul 23 2020 songnannan <songnannan2@huawei.com> - 2.28.3-1
- Type:enhancement
- Id:NA
- SUG:NA
- DESC: update to  2.28.3

* Mon Feb 24 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.22.2-6
- Type:enhancement
- Id:NA
- SUG:NA
- DESC:fix rpmbuild fail in make

* Thu Jan 23 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.22.2-5
- Type:enhancement
- Id:NA
- SUG:NA
- DESC:close build option gtkdoc

* Sat Jan 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.22.2-4
- Type:enhancement
- Id:NA
- SUG:NA
- DESC:optimization the spec

* Tue Dec 31 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.22.2-3
- Enable gtk-doc and go-introspection

* Fri Nov 8 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.22.2-2
- Modify cmake option to disable gtk-doc and go-introspection

* Wed Sep 18 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.22.2-1
- Package init
